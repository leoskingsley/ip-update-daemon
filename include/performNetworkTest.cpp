#include <curl.h>
#include "utils.hpp"

CURLcode utils::performNetworkTest(CURL *curl)  {
  if(curl) {
    CURLcode res;
    curl_easy_setopt(curl, CURLOPT_URL, "http://example.com");
    res = curl_easy_perform(curl);
    curl_easy_cleanup(curl);

    return res;
  }
}
