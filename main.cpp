#include <curl.h>
#include <iostream>
#include "include/utils.hpp"

int main() {
  std::cout << "Leah Skingsley 2018 - XHDA Group" << std::endl;

  CURL *curl = curl_easy_init();
  curl_easy_setopt(curl, CURLOPT_NOBODY,1);
  CURLcode testRes = utils::performNetworkTest(curl);
  if (testRes == CURLE_OK) {
    std::cout << "cURL Success" << std::endl;
  }

  return EXIT_SUCCESS;
}
